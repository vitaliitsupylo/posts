import * as React from "react";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import {Platform} from "react-native";
import {Ionicons} from "@expo/vector-icons";
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";

import {AppHeaderIcon} from "./../components/AppHeaderIcon";
import MainScreen from "./../screens/MainScreen";
import PostScreen from "./../screens/PostScreen";
import BookmarkedScreen from "./../screens/BookmarkedScreen";

import {THEME} from "./../theme";

const PostStack = createStackNavigator();
const BookedStack = createStackNavigator();
const Tab = createBottomTabNavigator();

function PostNavigator() {
	return (
		<PostStack.Navigator
			initialRouteName="Main"
			// headerMode="screen"
			screenOptions={{
				headerStyle: {
					backgroundColor:
						Platform.OS === "android" ? THEME.MAIN_COLOR : "#fff",
				},
				headerTintColor:
					Platform.OS === "android" ? "#fff" : THEME.MAIN_COLOR,
			}}
		>
			<PostStack.Screen
				name="Main"
				component={MainScreen}
				{...mainOption}
			/>
			<PostStack.Screen
				name="Post"
				component={PostScreen}
				{...postOption}
			/>
		</PostStack.Navigator>
	);
}

function BookedNavigator() {
	return (
		<BookedStack.Navigator
			// headerMode="screen"
			screenOptions={{
				headerStyle: {
					backgroundColor:
						Platform.OS === "android" ? THEME.MAIN_COLOR : "#fff",
				},
				headerTintColor:
					Platform.OS === "android" ? "#fff" : THEME.MAIN_COLOR,
			}}
		>
			<BookedStack.Screen name="Booked" component={BookmarkedScreen}/>
			<BookedStack.Screen
				name="Post"
				component={PostScreen}
				{...postOption}
			/>
		</BookedStack.Navigator>
	);
}

const mainOption = {
	options: ({route}) => ({
		title: "My home",
		headerRight: () => (
			<HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
				<Item
					title="Take Photo"
					iconName="ios-camera"
					onPress={() => {
						console.log("111");
					}}
				/>
			</HeaderButtons>
		),
		headerLeft: () => (
			<HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
				<Item
					title="Toggle menu"
					iconName="ios-menu"
					onPress={() => {
						console.log("111");
					}}
				/>
			</HeaderButtons>
		),
	}),
};
const postOption = {
	options: ({route}) => ({
		title: `Post ${route.params.post.id}`,
		headerStyle: {
			backgroundColor: THEME.DANGER_COLOR,
		},
		headerTintColor: "#fff",
		headerRight: () => (
			<HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
				<Item
					title="Take Photo"
					iconName={`${
						route.params.post.booked
							? "ios-star"
							: "ios-star-outline"
					}`}
					onPress={() => {
						console.log("111");
					}}
				/>
			</HeaderButtons>
		),
	}),
};

function BottomNavigator() {
	return (
		<Tab.Navigator
			tabBarOptions={{
				activeTintColor: THEME.MAIN_COLOR,
				inactiveTintColor: "gray",
			}}
		>
			<Tab.Screen
				name="Post"
				component={PostNavigator}
				options={{
					tabBarIcon: (info) => (
						<Ionicons
							name="ios-albums"
							size={info.size}
							color={info.color}
						/>
					),
				}}
			/>
			<Tab.Screen
				name="Booked"
				component={BookedNavigator}
				options={{
					tabBarIcon: (info) => (
						<Ionicons
							name="ios-star"
							size={info.size}
							color={info.color}
						/>
					),
				}}
			/>
		</Tab.Navigator>
	);
}

export const AppNavigation = () => {
	return (
		<NavigationContainer>
			<BottomNavigator/>
		</NavigationContainer>
	);
};
