import React from "react";
import { View, StyleSheet, Button, FlatList } from "react-native";

import { DATA } from "./../data";
import PostSmall from "./../components/PostSmall";

const MainScreen = ({ navigation }) => {
	const goToPost = (post) => navigation.navigate("Post", { post });

	return (
		<View style={styles.wrapper}>
			<FlatList
				data={DATA}
				keyExtractor={(post) => {
					post.id.toString();
				}}
				renderItem={({ item }) => (
					<PostSmall post={item} handler={goToPost} />
				)}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	wrapper: {
		padding: 10,
	},
});

export default MainScreen;
