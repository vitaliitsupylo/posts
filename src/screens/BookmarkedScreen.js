import React from "react";
import {View, StyleSheet, FlatList} from "react-native";

import {DATA} from "./../data.js";
import PostSmall from "./../components/PostSmall";

const BookmarkedScreen = ({navigation}) => {
	const goToPost = post => navigation.navigate("Post", {post});

	return (
		<View style={styles.wrapper}>
			<FlatList
				data={DATA}
				keyExtractor={post => post.id.toString()}
				renderItem={({item}) => item.booked ? <PostSmall post={item} handler={goToPost}/> : null}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	wrapper: {
		padding: 10,
	},
});

export default BookmarkedScreen;
