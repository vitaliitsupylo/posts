import React from "react";
import { View, Text, StyleSheet } from "react-native";

import Post from "./../components/Post";

const PostScreen = ({ route }) => {
  const { post } = route.params;
  return <Post post={post} />;
};

export default PostScreen;
