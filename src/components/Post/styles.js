import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	image: {
		width: "100%",
		height: 200,
	},
	textWrap: {
    padding: 10
  },
  title:{
    fontFamily: 'open-regular',
    fontSize: 18
  }
});

export default styles;
