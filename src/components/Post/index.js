import React from "react";
import { View, ScrollView, Text, Image, Button, Alert } from "react-native";

import { THEME } from "./../../theme";
import styles from "./styles";

const Post = ({ post }) => {
	const { img, text } = post;
	const removePost = () => {
		Alert.alert(
			"Delete this post",
			"Are you sure?",
			[
				{
					text: "Cancel",
					style: "cancel",
				},
				{
					text: "OK",
					style: "destructive",
					onPress: () => console.log("OK Pressed"),
				},
			],
			{ cancelable: false }
		);
	};

	return (
		<ScrollView>
			<Image style={styles.image} source={{ uri: img }} />
			<View style={styles.textWrap}>
				<Text style={styles.title}>{text}</Text>
			</View>
			<Button
				title="Delete"
				color={THEME.DANGER_COLOR}
				onPress={removePost}
			/>
		</ScrollView>
	);
};

export default Post;
